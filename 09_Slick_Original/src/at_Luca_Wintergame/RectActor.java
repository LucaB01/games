package at_Luca_Wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectActor {
		private double x,y;
		private int switchValue = 1;
		
		public RectActor (double x, double y) {
			super();
			this.x = x;
			this.y = y;
		}

		public void update (GameContainer gc, int delta) {
			switch (switchValue) {
			case 1:
				x++;
				if (x == 750) {
					switchValue++;
				}
				break;
			case 2:
				y++;
				if (y == 550) {
					switchValue++;
				}
				break;
			case 3:
				x--;
				if (x == 0) {
					switchValue++;
				}
				break;
			case 4:
				y--;
				if (y == 0) {
					switchValue = 1;
				}
				break;
				
			}
		}
		public void render (Graphics graphics) {
			graphics.drawRect((float)x, (float)y, 30, 30);
		}
}
