package at_Luca_Wintergame;

import java.util.Random;
import java.util.Timer;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class ShootingStar {
	private double x;
	private double y;

	
	public ShootingStar (double y, double x) {
		super();
		Random r = new Random();
		int val = r.nextInt(800);
		int val2 = r.nextInt(800);
		this.y = val;
		this.x = val2;
	}
	
	public void update (GameContainer gc, int delta) {
		move();
	}
	
	public void move () {
		y++;
		x++;
		if (x == 800) {
			Random r = new Random();
			int valX = r.nextInt(700);
			x = valX;
			Random r2 = new Random();
			int valY = r2.nextInt(500);
			y = -valY;
		}
		else if (y == 700) {
			Random r = new Random();
			int valX = r.nextInt(700);
			x = valX;
			Random r2 = new Random();
			int valY = r2.nextInt(500);
			y = -valY;
		}	
	}
		
	public void render (Graphics graphics) {
		graphics.setColor(Color.yellow);
		graphics.fillOval((float)x, (float)y, 20, 20);
	}
}
