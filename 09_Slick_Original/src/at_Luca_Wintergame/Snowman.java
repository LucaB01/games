package at_Luca_Wintergame;

import org.newdawn.slick.Color;
import org.newdawn.slick.Input;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowman {
	private double x;
	private double y;
	
	public Snowman (double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void update (GameContainer gc, int delta) {
		if (gc.getInput().isKeyPressed(Input.KEY_UP)) {
		      y = y - 20;
		    }
		if (gc.getInput().isKeyPressed(Input.KEY_DOWN)) {
		      y = y + 20;
		    }
		if (gc.getInput().isKeyPressed(Input.KEY_SPACE)) {
		      
		    }
	}
	
	public void render (Graphics graphics) {
		graphics.setColor(Color.white);
		graphics.fillOval((float)x+10, (float)y-30, 10, 10);
		graphics.fillOval((float)x+5, (float)y-20, 20, 20);
		graphics.fillOval((float)x, (float)y, 30, 30);
	}
}
