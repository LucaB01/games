package at_Luca_Wintergame;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowball {
	private int x;
	private double y;
	private int size;

	public Snowball (double y, int x, int size) {
		super();
		this.y = y;
		this.x = x;
		this.size = size;
	}

	public void update (GameContainer gc, int delta) {
		x++;
	}
	public void render (Graphics graphics) {
		graphics.setColor(Color.white);
		graphics.fillOval((float)this.x, (float)this.y, 5, 5);
	}
}

