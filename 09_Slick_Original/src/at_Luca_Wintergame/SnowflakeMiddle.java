package at_Luca_Wintergame;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class SnowflakeMiddle {
	private int x;
	private double y;
	private int size;

	public SnowflakeMiddle (double y, int x, int size) {
		super();
		Random r = new Random();
		int val = r.nextInt(700);
		int val2 = r.nextInt(700);
		this.y = val;
		this.x = val2;
		this.size = size;
	}

	public void update (GameContainer gc, int delta) {
		y = y + 0.6;
		if (y >= 650) {
			Random r = new Random();
			int val = r.nextInt(800);
			this.x = val;
			this.y = -50;
		}
	}
	public void render (Graphics graphics) {
		graphics.setColor(Color.white);
		graphics.fillOval((float)this.x, (float)this.y, 8, 8);
	}
}
