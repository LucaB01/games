package at_Luca_Wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CircleActor {
	private double x,y;
	private int switchValueOval = 1;
	
	public CircleActor (double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void update (GameContainer gc, int delta) {
		switch (switchValueOval) {
		case 1:
			x++;
			if (x == 750) {
				switchValueOval++;
			}
			break;
		case 2:
			x--;
			if (x == 0) {
				switchValueOval = 1;
			}
			break;
		}
	}
	public void render (Graphics graphics) {
		graphics.drawOval((float)x, (float)y, 30, 30);
	}
}
