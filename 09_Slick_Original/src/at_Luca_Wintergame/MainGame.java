package at_Luca_Wintergame;

import java.util.List;
import java.util.ArrayList;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class MainGame extends BasicGame {
	//private CircleActor circleActor;
	//private RectActor rectActor;
	//private OvalActor ovalActor;
	
	List<SnowflakeSmall> snowflakesSmall = new ArrayList<SnowflakeSmall>();
	List<SnowflakeMiddle> snowflakesMiddle = new ArrayList<SnowflakeMiddle>();
	List<SnowflakeBig> snowflakesBig = new ArrayList<SnowflakeBig>();
	private ShootingStar shootingStar;
	private Snowman snowman;
	List<Snowball> snowball = new ArrayList<Snowball>();

	public MainGame(String title) {
		super(title);
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		//this.circleActor.render(graphics);
		//this.rectActor.render(graphics);
		//this.ovalActor.render(graphics);
		for (SnowflakeSmall sfs : snowflakesSmall) {
			sfs.render(graphics);
		}
		for (SnowflakeMiddle sfm : snowflakesMiddle) {
			sfm.render(graphics);
		}
		for (SnowflakeBig sfb : snowflakesBig) {
			sfb.render(graphics);
		}
		
		this.shootingStar.render(graphics);
		this.snowman.render(graphics);
		for (Snowball sb : snowball) {
			sb.render(graphics);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		//this.circleActor = new CircleActor(100, 100);
		//this.rectActor = new RectActor(0, 0);
		//this.ovalActor = new OvalActor(200, 0);
		for (int i = 0; i < 50; i++) {	
			SnowflakeSmall sfs = new SnowflakeSmall(0, 0, 1);
			snowflakesSmall.add(sfs);
		}
		for (int i = 0; i < 50; i++) {	
			SnowflakeMiddle sfm = new SnowflakeMiddle(0, 0, 1);
			snowflakesMiddle.add(sfm);
		}
		for (int i = 0; i < 50; i++) {	
			SnowflakeBig sfb = new SnowflakeBig(0, 0, 1);
			snowflakesBig.add(sfb);
		}
		shootingStar = new ShootingStar(0, 0);
		snowman = new Snowman(10, 400);
		
		if (gc.getInput().isKeyPressed(Input.KEY_SPACE)) {
			Snowball sb = new Snowball(0, 0, 1);	//jetz brauche ich hier das aktuelle y vom Schneemann
			snowball.add(sb);
	    }
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		//this.circleActor.update(gc, delta);
		//this.rectActor.update(gc, delta);
		//this.ovalActor.update(gc, delta);
		for (SnowflakeSmall sfs : snowflakesSmall) {
			sfs.update(gc, 1);
		}
		for (SnowflakeMiddle sfm : snowflakesMiddle) {
			sfm.update(gc, 1);
		}
		for (SnowflakeBig sfb : snowflakesBig) {
			sfb.update(gc, 1);
		}
		this.shootingStar.update(gc, 1);
		this.snowman.update(gc, 1);
		for (Snowball sb : snowball) {
			sb.update(gc, 1);
		}
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
